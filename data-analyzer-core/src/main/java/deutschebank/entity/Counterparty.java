package deutschebank.entity;

import java.time.ZonedDateTime;

public class Counterparty {
    int id;
    String name;
    String status;
    ZonedDateTime dateRegistered;
}
