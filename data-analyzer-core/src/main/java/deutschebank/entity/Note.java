package deutschebank.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;

@XmlRootElement
public class Note {
    private int dealId;
    private String dealTime;
    private String dealType;
    private double dealAmount;
    private int dealQuantity;
    private String instrumentName;
    private String counterpartyName;
    private String counterpartyStatus;
    private String counterpartyDateRegistered;

    public int getDealId() {
        return dealId;
    }

    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    public String getDealTime() {
        return dealTime;
    }

    public void setDealTime(String dealTime) {
        this.dealTime = dealTime;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public double getDealAmount() {
        return dealAmount;
    }

    public void setDealAmount(double dealAmount) {
        this.dealAmount = dealAmount;
    }

    public int getDealQuantity() {
        return dealQuantity;
    }

    public void setDealQuantity(int dealQuantity) {
        this.dealQuantity = dealQuantity;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCounterpartyStatus() {
        return counterpartyStatus;
    }

    public void setCounterpartyStatus(String counterpartyStatus) {
        this.counterpartyStatus = counterpartyStatus;
    }

    public String getCounterpartyDateRegistered() {
        return counterpartyDateRegistered;
    }

    public void setCounterpartyDateRegistered(String counterpartyDateRegistered) {
        this.counterpartyDateRegistered = counterpartyDateRegistered;
    }
}
