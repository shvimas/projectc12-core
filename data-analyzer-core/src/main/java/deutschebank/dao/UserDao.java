package deutschebank.dao;

import deutschebank.dbutils.DBConnector;
import deutschebank.entity.Extra1Entity;
import deutschebank.entity.Extra2Entity;
import deutschebank.entity.Extra3Entity;
import deutschebank.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDao {
    private static final Logger LOGGER = Logger.getLogger(UserDao.class.getName());

    public User loadFromDB(String userid, String pwd) {
        User result = null;
        try {
            LOGGER.info("On Entry -> UserHandler.loadFromDB()");

            Connection connection = getConnection();
            PreparedStatement stmt = prepareStatement(userid, pwd, connection);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                result = new User(rs.getString("user_id"), rs.getString("user_pwd"));
                LOGGER.info(result.getUserID() + "//" + result.getUserPwd());
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }

        LOGGER.info("On Exit -> UserHandler.loadFromDB()");

        return result;
    }

    PreparedStatement prepareStatement(String userid, String pwd, Connection connection) throws SQLException {
        String sbQuery = "select * from db_grad_cs_1917.users where user_id=? and user_pwd=?";
        PreparedStatement stmt = connection.prepareStatement(sbQuery);
        stmt.setString(1, userid);
        stmt.setString(2, pwd);
        return stmt;
    }

    public List<Extra1Entity> getAveragePrices() {

        List<Extra1Entity> result = new ArrayList<>();
        LOGGER.info("average prices -> started");

        try {
            Connection conn = getConnection();
            String query = "select instrument_name, AVG_BUY, AVG_SELL from (\n" +
                    "\t\t(select deal_instrument_id, avg(deal_amount) as AVG_SELL from deal where deal_type = 'S' group by deal_instrument_id) as t1\n" +
                    "\t\tleft join\n" +
                    "\t\t(select deal_instrument_id, avg(deal_amount) as AVG_BUY from deal where deal_type = 'B' group by deal_instrument_id) as t2\n" +
                    "\t\t\tusing (deal_instrument_id)) left join instrument on instrument_id = deal_instrument_id;";
            PreparedStatement stmt = conn.prepareStatement(query);
            // set period
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Extra1Entity extra1Entity = new Extra1Entity();
                extra1Entity.setInstrumentName(rs.getString(1));
                extra1Entity.setAverageBuyPrice(rs.getFloat(2));
                extra1Entity.setAverageSellPrice(rs.getFloat(3));
                result.add(extra1Entity);
                LOGGER.info("Added " + extra1Entity.getInstrumentName() + " with values " +
                        extra1Entity.getAverageBuyPrice() + " " + extra1Entity.getAverageSellPrice());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LOGGER.info("average prices -> finished");

        return result;
    }

    public List<Extra2Entity> getEndPositions() {
        List<Extra2Entity> result = new ArrayList<>();
        LOGGER.info("end positions -> started");

        try {
            Connection conn = getConnection();
            String query =
                    "SELECT s.counterparty_name, s.instrument_name, (b.b - s.b) as ending_position\n" +
                            "        FROM (SELECT counterparty.counterparty_name, instrument.instrument_name, sum(deal.deal_quantity) as b\n" +
                            "        from db_grad_cs_1917.deal inner join db_grad_cs_1917.counterparty\n" +
                            "        on deal.deal_counterparty_id = counterparty.counterparty_id\n" +
                            "        inner join db_grad_cs_1917.instrument\n" +
                            "        on deal.deal_instrument_id = instrument.instrument_id\n" +
                            "        where deal_type = 'S' group by counterparty_name, instrument_name) as s\n" +
                            "        INNER JOIN (\n" +
                            "        SELECT counterparty.counterparty_name, instrument.instrument_name, sum(deal.deal_quantity) as b\n" +
                            "        from db_grad_cs_1917.deal inner join db_grad_cs_1917.counterparty\n" +
                            "        on deal.deal_counterparty_id = counterparty.counterparty_id\n" +
                            "        inner join db_grad_cs_1917.instrument\n" +
                            "        on deal.deal_instrument_id = instrument.instrument_id\n" +
                            "        where deal_type = 'B' group by counterparty_name, instrument_name) as b\n" +
                            "        ON s.counterparty_name = b.counterparty_name and s.instrument_name = b.instrument_name;";
            PreparedStatement stmt = conn.prepareStatement(query);
            // set period
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Extra2Entity ent = new Extra2Entity(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getDouble(3));
                result.add(ent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOGGER.info("end positions -> finished");

        return result;
    }

    public List<Extra3Entity> getProfitLoss() {

        List<Extra3Entity> result = new ArrayList<>();
        LOGGER.info("user profit/loss");

        try {
            Connection conn = getConnection();
            String query = "SELECT t1.counterparty_name,\n" +
                    "        (t1.b - t2.b) realised_PL\n" +
                    "        FROM (SELECT counterparty.counterparty_name, sum(deal.deal_amount) as b\n" +
                    "        from db_grad_cs_1917.deal inner join db_grad_cs_1917.counterparty\n" +
                    "        on deal.deal_counterparty_id = counterparty.counterparty_id\n" +
                    "        where deal_type = 'S' group by counterparty_name) as t1\n" +
                    "        INNER JOIN (SELECT counterparty.counterparty_name, sum(deal.deal_amount) as b\n" +
                    "        from db_grad_cs_1917.deal inner join db_grad_cs_1917.counterparty\n" +
                    "        on deal.deal_counterparty_id = counterparty.counterparty_id where deal_type = 'B'\n" +
                    "        group by counterparty_name) as t2\n" +
                    "        ON t1.counterparty_name = t2.counterparty_name;";
            PreparedStatement stmt = conn.prepareStatement(query);
            // set period
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Extra3Entity extra3Entity = new Extra3Entity();
                extra3Entity.setDealerName(rs.getString(1));
                extra3Entity.setProfitLoss(rs.getFloat(2));
                result.add(extra3Entity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        LOGGER.info("rProfitLoss finished");

        return result;
    }

    Connection getConnection() {
        return DBConnector.getConnection();
    }
}
