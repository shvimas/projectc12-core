package deutschebank.dao;

import deutschebank.dbutils.DBConnector;
import deutschebank.entity.Note;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class NoteDao {
    private DateTimeFormatter oldFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy-MM-dd HH:mm:ss");

    private final String QUERY = "SELECT deal.deal_id, deal.deal_time, deal.deal_type, deal.deal_amount, deal.deal_quantity, " +
            "instrument_name, counterparty_name, counterparty_status, counterparty_date_registered " +
            "from db_grad_cs_1917.deal " +
            "inner JOIN instrument on deal.deal_instrument_id = instrument.instrument_id " +
            "inner JOIN db_grad_cs_1917.counterparty on deal.deal_counterparty_id = counterparty.counterparty_id LIMIT ? OFFSET ?";

    public List<Note> findNoteListByOffsetAndLimit(long offset, long limit) {
        List<Note> result = new ArrayList<>();

        Connection con = DBConnector.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement(QUERY);

            ps.setLong(1, limit);
            ps.setLong(2, offset);
            rs = ps.executeQuery();

            while (rs.next()) {
                Note note = new Note();
                note.setDealId(rs.getInt("deal_id"));
                note.setDealTime(LocalDateTime.parse(rs.getString("deal_time"), oldFormatter).format(formatter));
                note.setDealType(rs.getString("deal_type"));
                note.setDealAmount(rs.getDouble("deal_amount"));
                note.setDealQuantity(rs.getInt("deal_quantity"));
                note.setInstrumentName(rs.getString("instrument_name"));
                note.setCounterpartyName(rs.getString("counterparty_name"));
                note.setCounterpartyStatus(rs.getString("counterparty_status"));
                note.setCounterpartyDateRegistered(rs.getDate("counterparty_date_registered").toString());

                result.add(note);
            }

            //close the resultset here
            try {
                rs.close();
            } catch (SQLException e) {
                //Do something here
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
