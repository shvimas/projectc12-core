package deutschebank.dbutils;

import org.junit.Assert;
import org.junit.Test;

import java.util.Properties;

public class DBConnectorTest {

    @Test
    public void initConnection_no_properties() {
        Properties properties = new Properties();
        properties.setProperty("Driver", "no such driver");
        DBConnector dbc = DBConnector.getDBConnector();
        Assert.assertNull(dbc.initConnection(properties));
    }

    @Test
    public void bootstrapDBConnection() {
    }
}