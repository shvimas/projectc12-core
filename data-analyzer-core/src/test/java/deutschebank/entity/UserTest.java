package deutschebank.entity;

import org.junit.Assert;
import org.junit.Test;

public class UserTest {

    @Test
    public void toJSON() {
        String expected = "{\"userID\":\"admin\",\"userPwd\":\"admin\"}";
        deutschebank.entity.User user = new deutschebank.entity.User("admin", "admin");
        Assert.assertEquals(expected, user.toJSON());
    }

    @Test
    public void equals() {
        deutschebank.entity.User user1 = new deutschebank.entity.User("name", "pwd");
        deutschebank.entity.User user2 = new deutschebank.entity.User("name", "pwd");
        Assert.assertEquals(user1, user2);
    }

    @Test
    public void getUserIdTest() {
        User user = new User("", "");
        String userId = user.getUserID();
        Assert.assertEquals("", userId);
    }

    @Test
    public void getUserPwdTest() {
        User user = new User("", "");
        String userPwd = user.getUserPwd();
        Assert.assertEquals("", userPwd);
    }

    @Test
    public void setUserIdTest() {
        User user = new User("", "");
        user.setUserID("234");
        Assert.assertEquals("234", user.getUserID());
    }

    @Test
    public void setUserPwdTest() {
        User user = new User("", "");
        user.setUserPwd("somepassword");
        Assert.assertEquals("somepassword", user.getUserPwd());
    }

}