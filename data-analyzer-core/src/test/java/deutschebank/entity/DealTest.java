package deutschebank.entity;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DealTest {
    Deal testDeal = new Deal();
    @Before
    public void setUp() throws Exception {
        testDeal.id = 1234;
        testDeal.date = ZonedDateTime.of(2018, 8, 10, 01,02,03,04, ZoneId.of("Europe/London"));
        testDeal.type = "t";
        testDeal.amount = 5600;
        testDeal.quantity = 1400;

    }

    @Test
    public void testId() {
        Assert.assertEquals(1234, testDeal.id);
    }

    @Test
    public void testDate() {
        Assert.assertEquals(2018, testDeal.date.getYear());
        Assert.assertEquals(Month.of(8), testDeal.date.getMonth());
        Assert.assertEquals(10, testDeal.date.getDayOfMonth());
        Assert.assertEquals(1, testDeal.date.getHour());
        Assert.assertEquals(2, testDeal.date.getMinute());
        Assert.assertEquals(3, testDeal.date.getSecond());
        Assert.assertEquals(4, testDeal.date.getNano());
        Assert.assertEquals(ZoneId.of("Europe/London"), testDeal.date.getZone());
    }

    @Test
    public void testType() {
        Assert.assertEquals("t", testDeal.type);
    }

    @Test
    public void testAmount() {
        Assert.assertEquals(5600,testDeal.amount,0);
    }

    @Test
    public void testQuantity() {
        Assert.assertEquals(1400, testDeal.quantity);
    }
}