package deutschebank.entity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.Assert.*;

public class CounterpartyTest {
    Counterparty testCounterparty;

    @Before
    public void setUp() throws Exception {
        testCounterparty = new Counterparty();
        testCounterparty.id = 0001;
        testCounterparty.name = "Deutsche Bank";
        testCounterparty.status = "f";
        testCounterparty.dateRegistered = ZonedDateTime.of(2018, 8, 10, 01,02,03,04, ZoneId.of("Europe/London"));
    }

    @Test
    public void testId() {
        Assert.assertEquals(0001, testCounterparty.id);
    }

    @Test
    public void testName() {
        Assert.assertEquals("Deutsche Bank", testCounterparty.name);
    }

    @Test
    public void testStatus() {
        Assert.assertEquals("f",testCounterparty.status);
    }

    @Test
    public void testDateRegistered() {
        Assert.assertEquals(2018, testCounterparty.dateRegistered.getYear());
        Assert.assertEquals(Month.of(8), testCounterparty.dateRegistered.getMonth());
        Assert.assertEquals(10, testCounterparty.dateRegistered.getDayOfMonth());
        Assert.assertEquals(1, testCounterparty.dateRegistered.getHour());
        Assert.assertEquals(2, testCounterparty.dateRegistered.getMinute());
        Assert.assertEquals(3, testCounterparty.dateRegistered.getSecond());
        Assert.assertEquals(4, testCounterparty.dateRegistered.getNano());
        Assert.assertEquals(ZoneId.of("Europe/London"), testCounterparty.dateRegistered.getZone());
    }

}